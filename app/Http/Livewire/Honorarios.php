<?php

namespace App\Http\Livewire;

use Illuminate\Http\Request;
use Livewire\Component;
use App\Models\Empleado;
use App\Models\Honorario;

class Honorarios extends Component
{
    public $empleado_id;
    public $honorarios;
    public $empleado;

    public function mount($empleado_id)
    {
        $this->empleado_id = $empleado_id;
    }

    public function render()
    {
        $this->empleado = Empleado::find($this->empleado_id);
        $this->honorarios = Honorario::where('empleado_id', $this->empleado_id)->get();
        return view('livewire.honorarios', [ 'empleado' => $this->empleado, 'honorarios' => $this->honorarios ])->layout('layouts.app');
    }
}
