<?php

namespace App\Http\Livewire;

use App\Models\Empleado;
use Livewire\Component;
use App\Models\Recibo;

class Recibos extends Component
{
    public $recibos;
    public $empleado_id;
    public $empleado;

    public function mount($empleado_id)
    {
        $this->empleado_id = $empleado_id;
    }

    public function render()
    {
        $this->recibos = Recibo::where('empleado_id', $this->empleado_id)->get();
        $this->empleado = Empleado::find($this->empleado_id);

        return view('livewire.recibos');
    }
}
