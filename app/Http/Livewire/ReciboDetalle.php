<?php

namespace App\Http\Livewire;

use App\Models\Empleado;
use App\Models\Honorario;
use App\Models\Recibo_Detalle;
use App\Models\Recibo;
use Livewire\Component;

class ReciboDetalle extends Component
{
    public $recibo;
    public $recibo_id;
    public $detalles;
    public $honorarios;
    public $empleado;

    public function mount($recibo_id)
    {
        $this->recibo_id = $recibo_id;
    }
    public function render()
    {
        $this->recibo = Recibo::find($this->recibo_id);
        $this->empleado = Empleado::find($this->recibo->empleado_id);
        $this->honorarios =  Recibo_Detalle::join('honorarios', 'honorarios.id', '=', 'recibos_detalle.honorario_id')
            ->where('recibos_detalle.recibo_id', $this->recibo_id)
            ->get(['honorarios.*']);

        return view('livewire.recibo-detalle');
    }
}
