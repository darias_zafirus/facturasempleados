<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Honorario extends Model
{
    use HasFactory;
    protected $fillable = [
        'empleado_id',
        'name',
        'moneda',
        'vigencia_desde'
    ];
}
