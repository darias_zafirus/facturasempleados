<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Recibo_Detalle extends Model
{
    protected $table = 'recibos_detalle';

    use HasFactory;
    protected $fillable = [
        'recibo_id',
        'honorario_id'
    ];
}
