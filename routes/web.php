<?php

use Illuminate\Support\Facades\Route;
use App\Http\livewire\Empleados;
use App\Http\livewire\Recibos;
use App\Http\livewire\Honorarios;
use App\Http\livewire\ReciboDetalle;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {
    Route::get('/empleados', Empleados::class)->name('empleados');
    Route::get('/honorarios/{empleado_id?}', Honorarios::class)->name('honorarios');
    Route::get('/recibos/{empleado_id?}', Recibos::class)->name('recibos');
    Route::get('/detalle/{recibo_id?}', ReciboDetalle::class)->name('recibos-detalles');
    Route::get('/generar-recibos', Empleados::class)->name('generar-recibos');
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
});
