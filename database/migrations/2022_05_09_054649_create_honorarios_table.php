<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHonorariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('honorarios', function (Blueprint $table) {
            $table->id();
            $table->foreignId('empleado_id')
                ->references('id')
                ->on('empleados')
                ->onDelete('cascade');;
            $table->string("name", 50);
            $table->string("moneda", 30);
            $table->double("importe");
            $table->date("vigencia_desde");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('honorarios');
    }
}
