<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecibosDetalleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recibos_detalle', function (Blueprint $table) {
            $table->id();
            $table->foreignId('recibo_id')
                ->references('id')
                ->on('recibos')
                ->onDelete('cascade');;
            $table->foreignId('honorario_id')
                ->references('id')
                ->on('honorarios')
                ->onDelete('cascade');;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recibos_detalle');
    }
}
