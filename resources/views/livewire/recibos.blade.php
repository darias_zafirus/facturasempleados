<div>
    <h3> Empleado: {{ $empleado->nombres }}, {{ $empleado->apellidos }} </h3>
    <table class=" table-fixed w-full ">
        <thead>
        <tr class=" bg-indigo-600 text-white ">
            <th class=" px-4 py-2 ">Periodo</th>
            <th class=" px-4 py-2 ">Fecha Pago</th>
            <th class=" px-4 py-2 ">Detalle</th>
        </tr>
        </thead>
        <tbody>
        @foreach($recibos as $recibo)
            <tr>
                <td class=" border px-4 py-2 ">{{ $recibo->Periodo }}</td>
                <td class=" border px-4 py-2 ">{{ $recibo->fecha_pago }}</td>
                <td class=" border px-4 py-2 "><a href="/detalle/{{ $recibo->id }}">Ver Detalle</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
