<div>
<table class=" table-fixed w-full ">
    <thead>
        <tr class=" bg-indigo-600 text-white ">
            <th class=" px-4 py-2 ">Nombres</th>
            <th class=" px-4 py-2 ">Apellidos</th>
            <th class=" px-4 py-2 ">Categoría</th>
            <th class=" px-4 py-2 ">Fecha Ingreso</th>
            <th class=" px-4 py-2 ">Fecha Baja</th>
            <th class=" px-4 py-2 ">Honorarios</th>
            <th class=" px-4 py-2 ">Recibos</th>
        </tr>
    </thead>
    <tbody>
        @foreach($empleados as $empleado)
        <tr>
            <td class=" border px-4 py-2 ">{{ $empleado->nombres }}</td>
            <td class=" border px-4 py-2 ">{{ $empleado->apellidos }}</td>
            <td class=" border px-4 py-2 ">{{ $empleado->categoria }}</td>
            <td class=" border px-4 py-2 ">{{ $empleado->fecha_ingreso }}</td>
            <td class=" border px-4 py-2 ">{{ $empleado->fecha_baja }}</td>
            <td class=" border px-4 py-2 "><a href="honorarios/{{ $empleado->id }}">Ver Honorarios</a></td>
            <td class=" border px-4 py-2 "><a href="recibos/{{ $empleado->id }}">Ver Recibos</a></td>
        </tr>
        @endforeach
    </tbody>
</table>
</div>
