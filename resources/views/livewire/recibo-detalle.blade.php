<div>
    <h3>
        Empleado: {{ $empleado->nombres }}, {{ $empleado->apellidos }} |
        Recibo Nro: {{ $recibo->id }} |
        Periodo: {{ $recibo->Periodo }}
    </h3>
    <table class=" table-fixed w-full ">
        <thead>
        <tr class=" bg-indigo-600 text-white ">
            <th class=" px-4 py-2 ">Nombre</th>
            <th class=" px-4 py-2 ">Moneda</th>
            <th class=" px-4 py-2 ">Importe</th>
        </tr>
        </thead>
        <tbody>
        @foreach($honorarios as $honorario)
            <tr>
                <td class=" border px-4 py-2 ">{{ $honorario->name }}</td>
                <td class=" border px-4 py-2 ">{{ $honorario->moneda }}</td>
                <td class=" border px-4 py-2 ">{{ $honorario->importe }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
